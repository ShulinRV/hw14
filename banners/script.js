const images = document.querySelectorAll(".image-to-show"); // find all img
const firstImage = images[0];
const lastImage = images[images.length - 1];
const stopButton = document.querySelector(".stop");
const starButton = document.querySelector(".start");

const slider = () => {
  const currentImage = document.querySelector(".visible");
  if (currentImage !== lastImage) {
    currentImage.classList.remove("visible");
    currentImage.nextElementSibling.classList.add("visible");
  } else {
    currentImage.classList.remove("visible");
    firstImage.classList.add("visible");
  }
};

let timer = setInterval(slider, 3000);

stopButton.addEventListener("click", () => {
  clearInterval(timer);
  starButton.disabled = false;
  stopButton.disabled = true;
});

starButton.addEventListener("click", () => {
  timer = setInterval(slider, 3000);
  starButton.disabled = true;
  stopButton.disabled = false;
});

const light = document
  .querySelector(".light")
  .addEventListener("click", (event) => {
    event.preventDefault();
    if (localStorage.getItem("theme") === "dark") {
      localStorage.removeItem("theme");
    } else {
      localStorage.setItem("theme", "dark");
    }
    addClassDark();
  });

function addClassDark() {
  try {
    if (localStorage.getItem("theme") === "dark") {
      document.querySelector("html").classList.add("dark");
    } else {
      document.querySelector("html").classList.remove("dark");
    }
  } catch (err) {}
}
addClassDark();
